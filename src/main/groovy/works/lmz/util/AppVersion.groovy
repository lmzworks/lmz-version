package works.lmz.util

/**
 *
 * @author: Richard Vowles - https://plus.google.com/+RichardVowles
 */
public interface AppVersion {
	String getVersion()
}